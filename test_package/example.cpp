#include <iostream>

#include <cstdlib>
#include <ctime>

#define cimg_display 0 
#include <CImg/CImg.h>

int main(int argc, const char** argv)
{
    cimg_usage("View the color profile of an image along the X axis");
    return 0;
}
