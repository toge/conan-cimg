import shutil

from conans import ConanFile, tools

class CimgConan(ConanFile):
    name           = "cimg"
    version        = "2.8.4"
    license        = "CeCILL-C FREE SOFTWARE LICENSE"
    url            = "https://bitbucket.org/toge/conan-cimg"
    description    = "The CImg Library is a small and open-source C++ toolkit for image processing http://cimg.eu"
    no_copy_source = True
    settings       = "os"

    def source(self):
        tools.get("https://github.com/dtschump/CImg/archive/v.{}.zip".format(self.version))
        shutil.move("CImg-v." + self.version, "CImg")

    def requirements(self):
        self.requires("libtiff/[>= 4.0.0]@bincrafters/stable")
        self.requires("zlib/[>= 1.2.8]@conan/stable")
        self.requires("libpng/[>= 1.6.32]@bincrafters/stable")
        self.requires("libjpeg/9b@bincrafters/stable")
        self.requires("openexr/[>= 2.3.0]@conan/stable")

    def package(self):
        self.copy("CImg.h", dst="include/CImg",         src="CImg")
        self.copy("*.h",    dst="include/CImg/plugins", src="CImg/plugins")

    def package_info(self):
        self.info.header_only()
        self.cpp_info.defines = ["cimg_use_tiff", "cimg_use_png", "cimg_use_jpeg", "cimg_use_zlib", "cimg_use_cpp11=1", "cimg_use_openexr"]
        if self.settings.os == "Linux":
            self.cpp_info.libs = ["X11", "xcb", "pthread"]
        elif self.settings.os == "Macos":
            self.cpp_info.libs = ["pthread"]
            for framework in ['Carbon',
                              'Cocoa',
                              'AudioToolbox',
                              'OpenGL',
                              'AVKit',
                              'AVFoundation',
                              'Foundation',
                              'IOKit',
                              'ApplicationServices',
                              'CoreText',
                              'CoreGraphics',
                              'CoreServices',
                              'CoreMedia',
                              'Security',
                              'ImageIO',
                              'System',
                              'WebKit']:
                self.cpp_info.exelinkflags.append('-framework %s' % framework)
            self.cpp_info.sharedlinkflags = self.cpp_info.exelinkflags
        else:
            self.cpp_info.libs = []
